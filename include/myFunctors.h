#include <cstddef> // for std::size_t
#include <array>   // for std::array

namespace otb {
namespace myFunctors {

template <class TInputPixelType, class TOutputPixelType>
struct MyFirstFunctorType
{
  // This functor has three input parameters: parameter1 and parameter2
  float m_Parameter1;
  float m_Parameter2;

  // Here we tell that the functor returns a 2-components output pixel
  static constexpr std::size_t m_NumberOfComponentsPerOutputPixel{2};

  std::size_t OutputSize(const std::array<size_t, 1>&) const
  {
    return m_NumberOfComponentsPerOutputPixel;
  }

  std::size_t OutputSize() const
  {
    return m_NumberOfComponentsPerOutputPixel;
  }

  TOutputPixelType operator()(TInputPixelType const& pix)
  {
    // We create an output pixel filled with zeros
    TOutputPixelType outPix(OutputSize());
    outPix.Fill(0);

    // We apply our very complicated formula
    // Here we compute the mean of the scaled input pixel components
    float sum = 0;
    float n = 0;
    for (unsigned int band_index = 0 ; band_index < pix.Size() ; band_index++)
      {
      sum += pix[band_index];
      n++;
      }
    float scaled_mean = m_Parameter1 * (sum / n) + m_Parameter2;

    // We store the values in the output pixel
    outPix[0] = scaled_mean;
    outPix[1] = n;

    // And finally we return the output pixel
    return outPix;
  }

  bool operator != (const MyFirstFunctorType& other)
  {
    if ( m_Parameter1 != other.m_Parameter1||
        m_Parameter2 != other.m_Parameter2 )
      return true;
    else return false;
  }
};


}
}
