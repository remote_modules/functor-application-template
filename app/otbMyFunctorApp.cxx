// For application
#include "otbWrapperApplication.h"
#include "otbWrapperApplicationFactory.h"

// For functors
#include "myFunctors.h"
#include "otbFunctorImageFilter.h"

namespace otb {
namespace Wrapper {

class MyFunctorApp : public Application
{
public:
  // Typedefs for the application
  typedef MyFunctorApp Self;
  typedef itk::SmartPointer<Self> Pointer;

  // Macros for the application
  itkNewMacro(Self);
  itkTypeMacro(MyFunctorApp, otb::Wrapper::Application);

  // Typedefs for our own functor
  // This is the declaration for the functor, aka the function that process pixels
  typedef otb::myFunctors::MyFirstFunctorType<FloatVectorImageType::PixelType,
      FloatVectorImageType::PixelType> MyFirstFunctorType;
  // This is the declaration for the functor filter, aka the filter that will
  // apply the functor "MyFirstFunctorType" on the image
  typedef otb::FunctorImageFilter<MyFirstFunctorType> MyFunctorFilterType;

private:

  void DoInit()
  {
    // Application description
    SetName("MyFunctorApp");
    SetDescription("Application description.");
    SetDocAuthors("Author name(s)");

    // Application parameters
    AddParameter(ParameterType_Float, "parameter1", "The parameter 1 of the application");
    AddParameter(ParameterType_Float, "parameter2", "The parameter 2 of the application");
    AddParameter(ParameterType_InputImage, "in" , "Input image");
    AddParameter(ParameterType_OutputImage, "out", "Output image");

  }

  void DoUpdateParameters()
  {
  }

  void DoExecute()
  {
    // Here, we "wire" the input to the filter, and the filter to the output

    // First we instanciate the filter
    m_Filter = MyFunctorFilterType::New();

    // We set the parameters 1 and 2 the the functor
    m_Filter->GetModifiableFunctor().m_Parameter1 = GetParameterFloat("parameter1");
    m_Filter->GetModifiableFunctor().m_Parameter2 = GetParameterFloat("parameter2");

    // Application input --> Filter
    m_Filter->SetInput(GetParameterFloatVectorImage("in"));

    // Filter --> Application output
    SetParameterOutputImage("out", m_Filter->GetOutput());

  }

  // We declare the functor filter here, as we need it alive during the application execution
  MyFunctorFilterType::Pointer m_Filter;
};

} // namespace otb
} // namespace wrapper

OTB_APPLICATION_EXPORT(otb::Wrapper::MyFunctorApp)
