# Functor application template

In OTB, *functors* are classes that aims to ease the coding of functions performing on images pixels.
This remote module provides a template to help C++ API newcomers to create their own *functor* based *OTB application*. 

## What's inside this module?

We put the minimal set of components that a developer needs to create its own first *OTB application*.
Here are the two main pieces of code:
 - A functor, defined in _include/myFunctors.h_. This very simple functor implement a function that computes something from a pixel.
 - An *OTB application*, defined in source code _app/otbMyFunctorApp.cxx_. The application apply the previously described *functor* to an input image, and produce an output image.

## How to build it?

First, build OTB.
Then, clone this repository, and build the module.

