set(DOCUMENTATION "My functor module template.")

# define the dependencies of the include module and the tests
otb_module(MyFunctorTutorial
  DEPENDS
    OTBCommon
    OTBApplicationEngine
  TEST_DEPENDS
    OTBTestKernel
    OTBCommandLine
  DESCRIPTION
    "${DOCUMENTATION}"
)

